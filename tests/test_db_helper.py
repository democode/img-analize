import unittest
import helpers.db_helper as db_helper

class testDbHelper(unittest.TestCase):

    def setUp(self):
        self.db = db_helper.dbHelper("unittestDB", "test_collection")

    # test connection database
    def test_connect_db(self):
        result = self.db
        self.assertNotEqual(result, None)

    # test insert data to collection
    def test_insert_data(self):
        data = {"info":"sometext"}
        result = self.db.insert_data(data)
        self.assertNotEqual(result, None)

    # test clear all data in collection
    def test_clear_all(self):
        result = self.db.clear_all()
        self.assertNotEqual(result, None)
