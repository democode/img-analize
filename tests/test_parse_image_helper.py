import unittest
import helpers.parse_image_helper as helper

class testParseImageHelper(unittest.TestCase):

    def setUp(self):
        pass


    #test list name from image file from src directory defult should had more than 1 file not 0
    def test_get_image_files(self):
        img = helper.parseImageHelper()
        result = img.get_image_files()
        self.assertNotEqual(len(result),0)

    #test set file name in list result not to be 0
    def test_set_image_files(self):
        img = helper.parseImageHelper()
        result = img.set_image_files(["test.jpg"])
        self.assertNotEqual(result,0)

    # test extract bitmap from image should not return 0
    def test_extract_bitmap(self):
        img = helper.parseImageHelper()
        result = img.extract_bitmap("src/imgs/image2.jpg")
        self.assertNotEqual(len(result),0)
