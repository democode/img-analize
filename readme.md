# Image Processing Project ( DEMO )
## Detail
This project is develop by python using Pillow library for process image to work with OpenCV

## Requirements
###[ Dependency Program] 
 - Docker (https://www.docker.com/community-edition#/download)
 - Docker-compose (https://docs.docker.com/compose/install/)
 - MongoDB Container (https://hub.docker.com/_/mongo/)
 - Python2.7 (https://www.python.org/downloads/)
 - Pip (https://pip.pypa.io/en/stable/installing)

###[ Dependency Library on Pip ]
 - virtualenv
 - pillow 
 - numpy
 - pymongo

## Installation
```
# activate virtual environment ( Linux/Unix )
source env/bin/activate
# activate virtual environment ( Window )
env/Scripts/activate

# install all libraries
pip install -r requirements.txt

# Build mongo database server from docker container  
docker-compose --file docker-compose.yml up -d

```
