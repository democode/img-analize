import helpers.parse_image_helper as img_helper
import feature.feature_simple as feature_simple
import helpers.db_helper as db_helper

def main():
    # get bitmap from selected image
    img = img_helper.parseImageHelper()
   
    # test image 24 *24 with 2 pictures
    # bitmap = img.extract_bitmap("src/imgs/imgying.jpg")
    bitmap = img.extract_bitmap("src/imgs/star.jpg")
    
    # show full size image in bitmap 
    img.dump_bitmap()

    # feature
    ### example code ###
    my_feature = feature_simple.featureSimple(bitmap)
    squares = my_feature.draw_square()
    my_feature.dump_square()

if __name__ == "__main__":
    main()
