import sys, os, numpy, pprint
from pymongo import MongoClient

class dbHelper():

    database = None
    # current connect session in mongodb
    connection = None
    # current collection
    selected = None

    def __init__(self, db_name, collection_name):
        self.connection = MongoClient("localhost", 5210)
        self.database = db_name
        db = self.connection[db_name]
        self.selected = db[collection_name]

    # insert data
    def insert_data(self, data):
        result = self.selected.insert_one(data)
        return result

    # clear all data in db
    def clear_all(self):
        result = self.selected.delete_many({})
        return result

    # drop current collection
    def drop_collection(self):
        result = self.selected.drop()
        return result

    # drop db
    def drop_db(self):
        result = self.connection.drop_database(self.database)
        return result
