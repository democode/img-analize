import sys, os, numpy, random, pprint
from PIL import Image

class scannerHelper():

    squares = []

    def __init__(self, bitmap):
        if len(bitmap) != None:
            self.bitmap = bitmap
            self.image_size  = len(bitmap)
            self.square_size = self.get_scale(self.image_size)
    
    def get_scale(self, size):
        scale = [size/guess_size for guess_size in range(1,size)]
        scale.sort()
        for compare_size in range(3,size/2):
            if compare_size in scale:
                scale = compare_size
                break
        return scale

    def draw_square(self):
        #cache all squares
        cache_all_square = []
        # incremet by square size position X
        for square_x in range(0, self.image_size, self.square_size):
            # cache one row from image
            cache_image_row = []
            # incremet by square size position Y       
            for square_y in range(0, self.image_size, self.square_size):
                # cache 1 square from image
                cache_square = []
                # increment per 1 util limit of current square size
                for x in range(square_x, square_x+3, 1):
                    # cache 1 row in square size
                    cache_row = []
                    for y in range(square_y, square_y+3, 1):
                        cache_row.append(self.bitmap[x][y])
                    cache_square.append(cache_row)
                cache_image_row.append(cache_square)
            cache_all_square.append(cache_image_row)
        self.squares = cache_all_square
        return self.squares

    # show all squares in beautiful format 
    def dump_square(self):
        print '\n ### FETCH SQUARE ###\n'
        for squares in self.squares:
            for row in squares:
                for square in row:
                    # for col, square_row in enumerate(square):
                    pprint.pprint(square)
                print '-'*15+'>square'
            print '#'*50+'row'