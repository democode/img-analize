import sys, os, numpy, pprint
from PIL import Image

class parseImageHelper():

    list_imgs = []
    bitmap = []
    def __init__(self):
        self.list_imgs = os.listdir("src/imgs")

    # get files name
    def get_image_files(self):
        return self.list_imgs

    # add new file in list
    def set_image_files(self, files):
        signal = 0
        if len(files) != 0 and type(files) == list:
            [self.list_imgs.append(f) for f in files]
            signal = 1
        return signal

    # extract bitmap from image
    def extract_bitmap(self, imgname):
        img = Image.open(imgname).convert("L")
        img_x = img.size[0]
        img_y = img.size[1]
        pixels = img.load()
        for y in range(img_y):
            bitrow = []
            for x in range(img_x):
                bitrow.append(pixels[x, y])
            self.bitmap.append(bitrow)
        return self.bitmap

    # create image from bitmap
    def make_vector_image(self, name, bitmap, size=None):
        width = len(bitmap[0])
        height = len(bitmap)
        path = "src/vec/"
        img = Image.new("L", (width, height))
        pixels = img.load()
        for i in range(img.size[0]):
            for j in range(img.size[1]):
                pixels[i, j] = bitmap[j][i]
        # set reset
        if (size != None):
            img.resize(size).save(path+name, "JPEG")
        else:
            img.save(path+name, "JPEG")
        return None

    # show beautiful bitmap table
    def dump_bitmap(self):
        column_numbers =''
        for column_number, blank in enumerate(self.bitmap):
            column_numbers += '[ '+str(column_number)+']' if len(str(column_number)) < 2 else '['+str(column_number)+']'
        print '\n\t'+column_numbers+'\n'
        print '\t'+'-'*len(self.bitmap*4)+'-'
        for row, bitmap_sources in enumerate(self.bitmap):
            result = ""
            for col, bitmap_source in enumerate(bitmap_sources):
                linenumber = '['+str(row)+']\t' if len(str(row)) < 2 else '['+str(row)+']\t'
                bitmap_source = str(bitmap_source)
                if len(bitmap_source) == 1:
                    result += '  '+bitmap_source+'|' if col != 0 else linenumber+'|  '+bitmap_source+'|'
                elif len(bitmap_source) == 2:
                    result += ' '+bitmap_source+'|' if col != 0 else linenumber+'| '+bitmap_source+'|'
                else:
                    result += bitmap_source+'|' if col != 0 else linenumber+'|'+bitmap_source+'|' 
            print result
            print '\t'+'-'*len(self.bitmap*4)+'-'
        print '\n'